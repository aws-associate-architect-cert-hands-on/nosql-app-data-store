import subprocess
from time import sleep

aws_commands = {
    'create-table': 'aws lambda invoke --function-name create-table response.json',
    'insert-items': 'aws dynamodb batch-execute-statement --statements file://../json/partiqlbatch.json',
    'retrieve-book': 'aws lambda invoke --function-name retrieve-book response.json',
    'create-index': 'aws lambda invoke --function-name create-index response.json',
    'retrieve-category': 'aws lambda invoke --function-name retrieve-category response.json',
    'update-item': 'aws lambda invoke --function-name update-item response.json',
    'remove-item': 'aws lambda invoke --function-name remove-item response.json',
    'delete-table': 'aws lambda invoke --function-name delete-table response.json'
}

response = 'Get-Content response.json'

def run(cmd):
    completed = subprocess.run(["powershell", "-Command", cmd], capture_output=True, check=True, text=True)
    return completed

if __name__ == '__main__':
    try:
        for k, v in aws_commands.items():
            info = run(v)
            if k == 'create-index':
                lambda_resp = run(response)
                print("Lambda function {} ran successfully!".format(k), lambda_resp, info.stdout, sep="\n")
                sleep(600)
            elif k == 'insert-items':
                print("Lambda function {} ran successfully!".format(k), info.stdout, sep="\n")
                sleep(60)
            else:
                lambda_resp = run(response)
                print("Lambda function {} ran successfully!".format(k), lambda_resp, info.stdout, sep="\n")
                sleep(60)
    except Exception as e:
        print("Error:", e)
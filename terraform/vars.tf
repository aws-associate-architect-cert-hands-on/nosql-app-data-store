variable "resource_name" {
  description = "Name of the resources created to finding them more easily in the AWS Console"
  type        = string
  default = "NoSQLDatastore"
}

variable "lambdas_name" {
  description = "Lambdas name dictionary to iterate in with teh terraform's lambda function"
  type        = map(string)
  default = {
    "create-table" = "01-create-table.zip",
    "retrieve-book"  = "02-retrieve-book.zip",
    "create-index" = "03-create-index.zip",
    "retrieve-category" = "04-retrieve-category.zip",
    "update-item" = "05-update-item.zip",
    "remove-item" = "06-remove-item.zip",
    "delete-table" = "07-delete-table.zip"
  }
}

variable "lambda_handler" {
  description = "Handler method to be invoked by the Lambda runtime"
  type = string
  default = "lambda_function.lambda_handler"  
}

variable "lambda_runtime" {
  description = "Programming language runtime identifier"
  type = string
  default = "python3.9"  
}
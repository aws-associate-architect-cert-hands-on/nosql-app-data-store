# Define cloud providers
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.21.0"
    }
  }
}

provider "aws" {

  default_tags {
    tags = {
      Databases = "${var.resource_name}"
    }
  }
}
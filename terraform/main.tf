# Lambda functions
resource "aws_lambda_function" "lambda_function" {
  for_each      = var.lambdas_name
  filename      = "../functions/${each.value}"
  function_name = each.key
  role          = aws_iam_role.lambdas_role.arn
  handler       = var.lambda_handler
  runtime       = var.lambda_runtime

  tags = {
    Name = "${each.key}"
  }
}
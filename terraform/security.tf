# Lambda Role
resource "aws_iam_role" "lambdas_role" {
  name = "${var.resource_name}_lambda_role"

  assume_role_policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect" : "Allow",
        "Principal" : {
          "Service" : "lambda.amazonaws.com"
        },
        "Action" : "sts:AssumeRole"
      }
    ]
  })
}

# Policies attached to the lambda role
resource "aws_iam_policy_attachment" "attach_lambda_policy" {
  name       = "${var.resource_name}_lambda_execution_policy"
  roles      = [aws_iam_role.lambdas_role.name]
  policy_arn = data.aws_iam_policy.AWSLambdaBasicExecutionRole.arn
}

resource "aws_iam_policy_attachment" "attach_dynamodb_policy" {
  name       = "${var.resource_name}_lambda_dynamodb_policy"
  roles      = [aws_iam_role.lambdas_role.name]
  policy_arn = data.aws_iam_policy.AmazonDynamoDBFullAccess.arn
}
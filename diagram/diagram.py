from diagrams import Diagram, Cluster, Edge
from diagrams.aws.compute import Lambda
from diagrams.aws.devtools import CLI
from diagrams.aws.database import DDB
from diagrams.aws.security import IAM
from diagrams.programming.language import Python

diag_config = {
  "fontsize": "45",
  "center": "true",
  "nodesep": "0.0",
  "ranksep": "0.6",
  "compound": "true",
  "concentrate": "true"
}

cluster_level_1 = {
 "fontsize": "24",
 "style": "bold",
 "labeljust": "l",
 "bgcolor": "transparent",
 "pencolor": "darkblue",
 "penwidth": "0.5"
}

cluster_level_2 = {
 "fontsize": "18",
 "pencolor": "darkblue",
 "penwidth": "0.5"
}

nodes_config = {
  "fontsize": "10",
  "fontcolor": "black",
  "fixedsize": "true",
  "width": "1",
  "labelloc": "b"
}

arrow_config = {
  "color": "darkblue",
  "labelfontsize": "10",
  "labelangle": "-4",
  "labeldistance": "12"
}

with Diagram(name="", show=False, filename="diagram", graph_attr=diag_config, node_attr=nodes_config, edge_attr=arrow_config):
  insert = Python("Insert Data")
  cli = CLI("AWS CLI")
    
  with Cluster("AWS Cloud", graph_attr=cluster_level_1):
    database = DDB("NoSQL Database")

    with Cluster("CRUD Functions", graph_attr=cluster_level_2):
      create = Lambda("Create")
      retrieve = Lambda("Retrieve")
      update = Lambda("Update")
      delete = Lambda("Delete")
      role = IAM("IAM Role")

  # Arrows Configuration
  cli >> create >> database
  cli >> Edge() << retrieve >> Edge() << database 
  cli >> update >> database
  cli >> delete >> database
  cli >> insert >> database
